FROM            scottw/alpine-perl
# zkoušel jsem perl:slim, ale neprošla mi instalace přes cpanm

MAINTAINER      jan-herout jan.herout@gmail.com

RUN \
       export http_proxy="http://internet-proxy-s1.cz.o2:8080"   \
       export https_proxy="http://internet-proxy-s1.cz.o2:8080"  \
    && cpanm  --force --notest https://gitlab.com/Herout/app-mdd-generator-dist/raw/master/dist/App-MDD-Generator-0.1.0.tar.gz 

WORKDIR metadata
VOLUME metadata


